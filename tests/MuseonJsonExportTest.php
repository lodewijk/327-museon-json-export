<?php

namespace Bolt\Extension\TwoKings\MuseonJsonExport\Tests;

use Bolt\Tests\BoltUnitTest;
use Bolt\Extension\TwoKings\MuseonJsonExport\MuseonJsonExportExtension;

/**
 * MuseonJsonExport testing class.
 *
 * @author Lodewijk evers <lodewijk@twokings.nl>
 */
class MuseonJsonExportTest extends BoltUnitTest
{
    /**
     * Ensure that the ExtensionName extension loads correctly.
     */
    public function testExtensionBasics()
    {
        $app = $this->getApp(false);
        $extension = new MuseonJsonExportExtension($app);

        $name = $extension->getName();
        $this->assertSame($name, 'MuseonJsonExport');
        $this->assertInstanceOf('\Bolt\Extension\ExtensionInterface', $extension);
    }

    public function testExtensionComposerJson()
    {
        $composerJson = json_decode(file_get_contents(dirname(__DIR__) . '/composer.json'), true);

        // Check that the 'bolt-class' key correctly matches an existing class
        $this->assertArrayHasKey('bolt-class', $composerJson['extra']);
        $this->assertTrue(class_exists($composerJson['extra']['bolt-class']));

        // Check that the 'bolt-assets' key points to the correct directory
        $this->assertArrayHasKey('bolt-assets', $composerJson['extra']);
        $this->assertSame('web', $composerJson['extra']['bolt-assets']);
    }
}
